class VkAPIMock:
    def __init__(self):
        self.status = 'OK'

    def base_info(self, *args, **kwargs):
        pass

    def friends(self, *args, **kwargs):
        pass

    def common_friends(self, *args, **kwargs):
        pass

    def deep_friends(self, *args, **kwargs):
        pass

    def from_where_gender(self, *args, **kwargs):
        pass
