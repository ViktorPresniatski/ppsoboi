import unittest
from collections import defaultdict
from unittest import mock
from unittest.mock import MagicMock

from producers import produce_users, produce_user_connections


class KafkaProducerMock:
    def __init__(self, *args, **kwargs):
        self.messages = defaultdict(list)

    def send(self, topic, item):
        self.messages[topic].append(item)
        return MagicMock()

    def flush(self):
        pass


class TestKafka(unittest.TestCase):
    @mock.patch('producers.KafkaProducer')
    def test_users_producers(self, mock_kafka_producer):
        kafka_mock = KafkaProducerMock()
        mock_kafka_producer.return_value = kafka_mock
        mock_kafka_producer.return_value.send = kafka_mock.send
        produce_users(is_test=True)
        self.assertEqual(len(kafka_mock.messages['users']), 4039)

    @mock.patch('producers.KafkaProducer')
    def test_user_connections_producers(self, mock_kafka_producer):
        kafka_mock = KafkaProducerMock()
        mock_kafka_producer.return_value = kafka_mock
        mock_kafka_producer.return_value.send = kafka_mock.send
        produce_user_connections(is_test=True)
        self.assertEqual(len(kafka_mock.messages['user_connections']), 4039)
