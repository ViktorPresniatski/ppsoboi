import unittest
from unittest import mock
from mapper import main as map
from reducer import main as reduce

import sys
from contextlib import contextmanager
from io import StringIO


@contextmanager
def captured_output():
    new_out, new_err = StringIO(), StringIO()
    old_out, old_err = sys.stdout, sys.stderr
    try:
        sys.stdout, sys.stderr = new_out, new_err
        yield sys.stdout, sys.stderr
    finally:
        sys.stdout, sys.stderr = old_out, old_err


class TestMock:
    @staticmethod
    def map_stdin_mock():
        return ['1,2', '1,3', '2,3']

    @staticmethod
    def reduce_stdin_mock():
        return ['1\t1',
               '1\t1',
               '2\t1',
               '2\t1',
               '3\t1',
               '3\t1']

class TestMapReduce(unittest.TestCase):
    @mock.patch('mapper.sys.stdin', TestMock.map_stdin_mock())
    def test_map(self):
        with captured_output() as (out, err):
            map()

        output = out.getvalue()
        self.assertEqual(output, '1\t1\n'
                                 '2\t1\n'
                                 '1\t1\n'
                                 '3\t1\n'
                                 '2\t1\n'
                                 '3\t1\n')

    @mock.patch('reducer.sys.stdin', TestMock.reduce_stdin_mock())
    def test_reduce(self):
        with captured_output() as (out, err):
            reduce()

        output = out.getvalue()
        self.assertEqual(output, '1\t2\n'
                                 '2\t2\n'
                                 '3\t2\n')


if __name__ == '__main__':
    unittest.main()
