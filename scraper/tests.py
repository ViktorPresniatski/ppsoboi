import unittest
from unittest import mock

from scraper.mock import VkAPIMock
from scraper.scraper import get_vk_instance


class ScraperTest(unittest.TestCase):

    @mock.patch('scraper.scraper.VkFriends')
    def test_ok(self, mock_api):
        mock_api.return_value = VkAPIMock()
        vk = get_vk_instance('token', '123', 'v1', 2)

        ids = [1,2,3]
        vk.base_info(ids)
        self.assertEqual(vk.status, 'OK')

        vk.friends(ids[1])
        self.assertEqual(vk.status, 'OK')

        vk.common_friends()
        self.assertEqual(vk.status, 'OK')

        vk.deep_friends(deep=2)
        self.assertEqual(vk.status, 'OK')

        vk.from_where_gender()
        self.assertEqual(vk.status, 'OK')
