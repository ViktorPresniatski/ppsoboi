import unittest
from typing import NamedTuple
from unittest import mock

import consumers
from consumers import consume_user_connections, consume_users


class Message(NamedTuple):
    value: bytes


class DBMock:
    def __init__(self, table=None):
        self.db = {}

    def get_item(self, Key=None):
        return self.db.get(Key['user_id']) or {}

    def put_item(self, Item=None):
        self.db[Item['user_id']] = Item


class KafkaConsumerMock:
    def __init__(self, *args, topic=None, **kwargs):
        self.topic = topic

        self.users = [
            Message(value=b'0,3,6,1,1,1,4,2,1665,0'),
            Message(value=b'1,228,905,2,34,12,43,5,33,0'),
            Message(value=b'2,22,5,24,4,222,42,1,13,0'),
        ]
        self.user_connections = [
            Message(value=b'0\t23'),
            Message(value=b'1\t34'),
            Message(value=b'2\t43'),
        ]

    def __iter__(self):
        if self.topic == 'users':
            return iter(self.users)

        return iter(self.user_connections)


class TestKafka(unittest.TestCase):

    @mock.patch('consumers.KafkaConsumer')
    def test_users_consumer(self, mock_kafka_consumer):
        kafka_mock = KafkaConsumerMock(topic='users')
        mock_kafka_consumer.return_value = kafka_mock

        db_storage = DBMock(table='users')

        with mock.patch.object(consumers, 'table', db_storage):
            consume_users()

        self.assertDictEqual(db_storage.db, {
            0: {
                'user_id': 0,
                'wall_comments_count': 3,
                'mentions_received': 6,
                'shared_posts_recevied': 1,
                'mentions_sent': 1,
                'shared_posts_sent': 1,
                'posts': 4,
                'photos_count': 2,
                'audio_count': 1665,
            },
            1: {
                'user_id': 1,
                'wall_comments_count': 228,
                'mentions_received': 905,
                'shared_posts_recevied': 2,
                'mentions_sent': 34,
                'shared_posts_sent': 12,
                'posts': 43,
                'photos_count': 5,
                'audio_count': 33,
            },
            2: {
                'user_id': 2,
                'wall_comments_count': 22,
                'mentions_received': 5,
                'shared_posts_recevied': 24,
                'mentions_sent': 4,
                'shared_posts_sent': 222,
                'posts': 42,
                'photos_count': 1,
                'audio_count': 13,
            }
        })

    @mock.patch('consumers.KafkaConsumer')
    def test_user_connections_consumer(self, mock_kafka_consumer):
        kafka_mock = KafkaConsumerMock(topic='user_connections')
        mock_kafka_consumer.return_value = kafka_mock

        db_storage = DBMock(table='user_connections')

        with mock.patch.object(consumers, 'table', db_storage):
            consume_user_connections()

        self.assertDictEqual(db_storage.db, {
            0: {
                'user_id': 0,
                'friends_count': 23,
            },
            1: {
                'user_id': 1,
                'friends_count': 34,
            },
            2: {
                'user_id': 2,
                'friends_count': 43,
            }
        })
